<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Entities\User;
use App\Notifications\PhotoProcessed;
use App\Services\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $photo;
    private $user;

    public function __construct(Photo $photo, User $user)
    {
        $this->photo = $photo;
        $this->user = $user;
    }

    public function handle(PhotoService $photoService)
    {
        $sizes = [100, 150, 250];
        $this->photo->status = "PROCESSING";

        foreach ($sizes as $size) {
            $cropImagePath = $photoService->crop($this->photo->original_photo, $size, $size);
            $cropImagePath = str_replace('cropped', 'images/' . $this->user->id, $cropImagePath);
            $this->photo['photo_' . $size . '_' . $size] = $cropImagePath;
        }

        $this->photo->save();

        $this->user->notify(new PhotoProcessed($this->photo));
     }
}
