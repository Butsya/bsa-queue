<?php

namespace App\Jobs;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Failed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    public function handle()
    {
        throw new \Exception('Job is failed');
    }

    public function failed(\Exception $exception)
    {
        $this->photo->status = 'FAIL';

        $this->photo->save();
    }
}
