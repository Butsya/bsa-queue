<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Entities\User;
use App\Jobs\CropJob;
use App\Jobs\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    public function store(Request $request)
    {
        $user = User::find(auth()->user()->id);

        $path = Storage::putFileAs(
            'images/' . $user->id,
            $request->file('photo'),
            $request->file('photo')->getClientOriginalName(),
            'public'
        );

        $photo = new Photo();
        $photo->original_photo = $path;
        $photo->user_id = auth()->user()->id;
        $photo->status = 'UPLOADED';

        $photo->save();

        CropJob::dispatch($photo, $user)->onConnection('sync');

        return response()->json(['path' => $path], 200);
    }

    public function failJob()
    {
        Failed::dispatch();

        return response()->json([], 200);
    }
}
