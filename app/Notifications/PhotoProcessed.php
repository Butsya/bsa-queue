<?php


namespace App\Notifications;


use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class PhotoProcessed extends Notification implements ShouldQueue
{
    use Queueable;

    private $photo;

    /**
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->photo->status = "SUCCESS";
        $this->photo->save();
    }
}